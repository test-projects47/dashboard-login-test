"use client"
import {useState} from "react";

const Home = () => {
    const [email, setEmail] = useState('jessica.williamson@gmail.com')
    const [password, setPassword] = useState('password')

    // @ts-ignore
    const login = async (credentials) => {
        return await fetch('https://chingu-dashboard-be-development.up.railway.app/api/v1/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            body: JSON.stringify(credentials)
        })
    }

    const logout = async () => {
        return await fetch('http://localhost:8000/api/v1/auth/logout', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        })
    }

    // @ts-ignore
    const handleSubmit = async (e) => {
        e.preventDefault()
        const token = await login({
            email,
            password
        })
        console.log("token", token)
    }

    return (
        <>
            <form className="bg-white" onSubmit={handleSubmit}>
                <label className="block text-sm font-medium leading-6 text-gray-900">
                    <p>Email</p>
                    <input type="text"
                           value={email}
                           onChange={e => setEmail(e.target.value)}
                           className="block flex-1 border-1 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                    />
                </label>
                <label className="block text-sm font-medium leading-6 text-gray-900">
                    <p>Password</p>
                    <input type="password"
                           value={password}
                           onChange={e => setPassword(e.target.value)}
                           className="block flex-1 border-1 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6"
                    />
                </label>
                <div>
                    <button
                        className="rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                        type="submit"
                    >Login
                    </button>
                </div>
            </form>
            <button
                className="rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                onClick={logout}
            >Logout
            </button>
        </>
    )
}

export default Home